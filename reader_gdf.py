
import mne
from scipy.signal import butter, lfilter
import numpy as np
import pickle
import struct


''' ######### OPTIONAL : STOP BOTH MNE PRINTING AND PYTHON WARNINGS ############ '''
import warnings 
import sys, os

from contextlib import contextmanager

warnings.filterwarnings("ignore")

@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:  
            yield
        finally:
            sys.stdout = old_stdout

''' ############################################################################# '''

def read_gdf(pass_band = None,
	stim_codes = np.array([33041]), 
	segment_length = 3, 
	segment_offset = 0,
	class_labels_to_keep = None,
	order = 5,
	saving_path = None,
	num_subject = None,
	nb_run = None
	):

	'''
	input :
		- gate_to_data {string} : path the the .gdf data file we want to extract
	'''

	print('num subject : ', num_subject)
	EEG_signals_filtered_concatenated = None

	# First concatenate the 4 runs
	for i in range(1,nb_run+1):
		filename = 'data_store/curiosity_gdf/subject_' + str(num_subject) + '/record-ID' + str(num_subject) + '_S1_R' + str(i) + '.gdf'
		EEG_signals_filtered = extract_filtered_data(filename, pass_band, stim_codes, segment_length, segment_offset, class_labels_to_keep, order)
		if i == 1 :
			EEG_signals_filtered_concatenated = EEG_signals_filtered

		else :
			EEG_signals_filtered_concatenated.x = np.concatenate((np.array(EEG_signals_filtered_concatenated.x), np.array(EEG_signals_filtered.x)),axis = 2)
			EEG_signals_filtered_concatenated.y = np.concatenate((np.array(EEG_signals_filtered_concatenated.y), np.array(EEG_signals_filtered.y)),axis = 0)
			EEG_signals_filtered_concatenated.g = np.concatenate((np.array(EEG_signals_filtered_concatenated.g), np.array(EEG_signals_filtered.g)),axis = 0)

	return EEG_signals_filtered_concatenated


def band_pass_transform(band, order, fs) :
	'''
	input :
		- EEG_signals : object containg the data, labels and frequency
		- pass_band {tuple} [1*2] : a tuple containing two value : pass_band_low and pass_band_high
	'''
	
	#print('we band-pass filter all signals in ' + str(band[0]) + '_' + str(band[1]) + ' Hz')
	low_freq = band[0] * (2/fs)
	high_freq = band[1] * (2/fs)
	B, A = butter(order, [low_freq, high_freq], btype='band')

	return B, A
		

def extract_filtered_data(filename, pass_band, stim_codes, segment_length, segment_offset, class_labels_to_keep, order):

	# Create an EEGSignals object, as in the .mat (empty)
	EEG_signals = EEG_Signals()

	#with suppress_stdout(): # a way to not see what mne is printing
	#reading the gdf file from the path we got in input

	raw = mne.io.read_raw_edf(input_fname = filename)
	# Create an EEGEvents object (empty)
	EEG_events = EEG_Events(mne.io.find_edf_events(raw))
	picks = mne.pick_types(raw.info, meg=False, eeg=True, exclude='bads')

	data, times = raw[picks]

	data = data.T # to get the same format as matlab

	EEG_signals.s = raw.info['sfreq']

	position_channel_to_keep = [i for i in range(63,67)] #64 - electrode de reference
	data = data[:,position_channel_to_keep]
	


	window_length = np.floor((segment_length) * EEG_signals.s)
	offset = np.floor((segment_offset/1000) * EEG_signals.s)
	nb_channels = np.shape(data)[1]

	# Check list of channels and the final number of channels
	EEG_signals.c = EEG_events.chn

	# defining the parameter of a butterworth if need
	if pass_band != None:
		B, A = band_pass_transform(pass_band, order, EEG_signals.s)

	# checking for nans 
	if np.sum(np.sum(np.isnan(data))) > 0:
		print ('WARNING: there are some NaN values in the data !')

	#if required, ban-pass filter the signal in a given frequency ban using a butterworth filter
	if pass_band != None :
		s = lfilter(B, A, data, axis = 0)

	# counting the total number of trials for the kept classes
	mask = np.in1d(EEG_events.typ, stim_codes)
	nb_trials = np.count_nonzero(EEG_events.typ[mask])
	EEG_signals.y = np.zeros((nb_trials))
	EEG_signals.g = np.array(np.zeros((nb_trials))).astype(int)
	
	EEG_signals.x = np.zeros((int(window_length), nb_channels, nb_trials))
	current_trial = 0

	for e in range(len(EEG_events.typ)):
		code = EEG_events.typ[e]
		mask = np.in1d(code,stim_codes)


		if mask[0] == True:
			#pos = EEG_events.pos[e]  
			x = 0 
			if EEG_events.typ[e+1+x] == 33025 :
				x = 2
			if EEG_events.typ[e+1+x] == 33042 :

				lenght_question = EEG_events.pos[e+1+x] - EEG_events.pos[e+x] # Not used yet, but can work
				#print(lenght_question)
				pos = EEG_events.pos[e+1+x] - window_length # -window_length because we want the 3 last seconds, and not the 3 

				range_ = pos + np.arange(offset, (offset + window_length ))
				#range_ = np.arange(begin,end)
				if range_[-1] > np.shape(s)[0]: # checking that the data is there, if not stopping here
					print('Warning file ' + filename + ': (at least) one trial is incomplete. Stopping the reading here. Trials may be missing')

				EEG_signals.x[:,:,current_trial] = s[np.array(range_.astype(int)),:]


				if EEG_events.typ[e+2+x] == 33043 :
					EEG_signals.y[current_trial] = 1
				else:
					EEG_signals.y[current_trial] = 0

				if np.in1d(np.array([33026,33027,33028,33029,33030,33031,33032]),EEG_events.typ[e+2+x]).any():
					EEG_signals.g[current_trial] = grade_assination(EEG_events.typ[e+2+x])
				
				elif EEG_events.typ[e+2+x] == 33043:

					if EEG_events.typ[e+6+x]==33045: # Questionnaires
						EEG_signals.g[current_trial] = grade_assination(EEG_events.typ[e+7+x])

					elif np.in1d(np.array([33026,33027,33028,33029,33030,33031,33032]),EEG_events.typ[e+6+x]).any():
						EEG_signals.g[current_trial] = grade_assination(EEG_events.typ[e+6+x])

				elif EEG_events.typ[e+2+x] == 33044 and EEG_signals.g[current_trial] == 0:

					if np.in1d(np.array([33026,33027,33028,33029,33030,33031,33032]),EEG_events.typ[e+3+x]).any():
						EEG_signals.g[current_trial] = grade_assination(EEG_events.typ[e+3+x])
					elif EEG_events.typ[e+3+x] == 33045: # Questionnaires
						EEG_signals.g[current_trial] = grade_assination(EEG_events.typ[e+4+x])
					elif EEG_events.typ[e+3+x] == 33025: # keyboard
					
						if EEG_events.typ[e+5+x] == 33025:
							EEG_signals.g[current_trial] = grade_assination(EEG_events.typ[e+7+x])
						else:
							EEG_signals.g[current_trial] = grade_assination(EEG_events.typ[e+5+x])
			


					else:
						print('problem heeerrre')
						EEG_signals.g[current_trial] = 4

				else:
					print('no grade')
				


			else:
				if np.in1d(np.array([33026,33027,33028,33029,33030,33031,33032]),EEG_events.typ[e+1+x]).any():
					EEG_signals.g[current_trial] = grade_assination(EEG_events.typ[e+1+x])
				else:
					print('Grade assinations issues !!')
				print('')
				print('')
			current_trial += 1


	# if a trial was incomplete, remove it and the subseuqent ones
	if current_trial < nb_trials :
		print('There was an incomplete trial so this data is likely to be shorter than expected')
		EEG_signals.x = EEG_signals.x[:,:,0:current_trial]
		EEG_signals.y = EEG_signals.y[0:current_trial]

	return EEG_signals


def grade_assination(stimu):
	if stimu == 33026 :
		return 1
	elif stimu == 33027 :
		return 2
	elif stimu == 33028 :
		return 3
	elif stimu == 33029 :
		return 4
	elif stimu == 33030 :
		return 5
	elif stimu == 33031 :
		return 6
	elif stimu == 33032 :
		return 7

class EEG_Signals:

	def __init__(self) :
		self.s = None
		self.y = None
		self.x = None
		self.c = None
		self.g = None # Grade



class EEG_Events :

	def __init__(self, structure):
		self.nb =  structure[0] # number of all event in the file
		self.pos = structure[1] # Beginning of the events in samples
		self.typ = structure[2] # the event identifiers
		self.chn = structure[3] # the associatedchannels (0 for all)
		self.dur = structure[4] # the duration of the events


